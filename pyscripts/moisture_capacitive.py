import json

import board
from adafruit_seesaw.seesaw import Seesaw

i2c_bus = board.I2C()
ss = Seesaw(i2c_bus, addr=0x36)

if __name__ == "__main__":
    reading = {
        "moisture": ss.moisture_read(),
        "temp_c": ss.get_temp()
    }

    print(json.dumps(reading))
