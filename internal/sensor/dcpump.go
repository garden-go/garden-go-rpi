package sensor

import (
	"fmt"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
)

type DcPump struct {
	pinA rpio.Pin
	pinB rpio.Pin
	pinE rpio.Pin
}

func NewDcPump(pinANumber int, pinBNumber int, pinENumber int) DcPump {
	pinA := rpio.Pin(uint8(pinANumber))
	pinB := rpio.Pin(uint8(pinBNumber))
	pinE := rpio.Pin(uint8(pinENumber))

	dcPump := DcPump{
		pinA: pinA,
		pinB: pinB,
		pinE: pinE,
	}

	return dcPump
}

func (dc DcPump) Pump(ml int) {
	rpio.Open()

	dc.pinE.Pwm()
	dc.pinE.DutyCycle(1, 1)
	dc.pinE.Freq(100)

	dc.pinA.Output()
	dc.pinB.Output()
	dc.pinE.Output()

	dc.pinA.Low()
	dc.pinB.High()
	dc.pinE.High()

	fmt.Println("sleeping for: ", time.Duration(getPumpTime(ml))*time.Second)

	time.Sleep(time.Duration(getPumpTime(ml)) * time.Second)

	dc.pinA.Low()
	dc.pinB.Low()
	dc.pinE.Low()

	rpio.Close()
}

func getPumpTime(ml int) int {
	flowRate := 100 / 1 // ml per minute

	pumpTimeMin := float64(ml) / float64(flowRate)
	pumpTimeSec := pumpTimeMin * 60.0

	return int(pumpTimeSec)
}
