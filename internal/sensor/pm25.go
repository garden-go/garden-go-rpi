package sensor

import (
	"encoding/json"
)

const PyScriptAirQuality = "pm25.py"

type AirQualityReading struct {
	Pm10std  float32 `json:"pm10std"`
	Pm25std  float32 `json:"pm25std"`
	Pm100std float32 `json:"pm100std"`
	Pm10env  float32 `json:"pm10env"`
	Pm25env  float32 `json:"pm25env"`
	Pm100env float32 `json:"pm100env"`
	P03um    float32 `json:"p03um"`
	P05um    float32 `json:"p05um"`
	P10um    float32 `json:"p10um"`
	P25um    float32 `json:"p25um"`
	P50um    float32 `json:"p50um"`
	P100um   float32 `json:"p100um"`
}

func ReadAirQuality() (float32, float32, float32, float32, float32, float32) {

	airQualityReading := AirQualityReading{}
	results := runPyScript(PyScriptAirQuality)

	err := json.Unmarshal([]byte(results), &airQualityReading)
	if err != nil {
		panic(err)
	}

	return airQualityReading.P03um, airQualityReading.P05um, airQualityReading.P10um, airQualityReading.P25um, airQualityReading.P50um, airQualityReading.P100um
}
