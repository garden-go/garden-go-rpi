package sensor

import (
	"fmt"

	bh1750 "github.com/d2r2/go-bh1750"
	i2c "github.com/d2r2/go-i2c"
)

func ReadLight() int {

	// Create new connection to i2c-bus on 1 line with address 0x23.
	// Use i2cdetect utility to find device address over the i2c-bus
	i2c, err := i2c.NewI2C(0x23, 1)
	if err != nil {
		fmt.Println(err)
	}
	defer i2c.Close()

	sensor := bh1750.NewBH1750()
	// Reset sensor
	err = sensor.Reset(i2c)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(sensor.GetDefaultSensivityFactor())

	// Reset sensitivity factor to default value
	err = sensor.ChangeSensivityFactor(i2c, sensor.GetDefaultSensivityFactor())
	if err != nil {
		fmt.Println(err)
	}

	resolution := bh1750.HighestResolution
	amb, err := sensor.MeasureAmbientLight(i2c, resolution)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(amb)

	fmt.Println("Ambient light (%s) = %v lx", resolution, amb)

	return int(amb)
}
