package sensor

import (
	"encoding/json"
)

const PyScriptRainfall = "rainfall.py"

type RainfallReading struct {
	Rainfall float32 `json:"rainfall"`
	Unit     string  `json:"unit"`
}

func ReadRainfall() (float32, string) {

	reading := RainfallReading{}
	results := runPyScript(PyScriptRainfall)

	err := json.Unmarshal([]byte(results), &reading)
	if err != nil {
		panic(err)
	}

	return reading.Rainfall, reading.Unit
}
