# https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi
apt update -y
apt upgrade -y
apt install -y python3-pip git
pip3 install --upgrade setuptools

pip3 install adafruit-circuitpython-seesaw
pip3 install adafruit-circuitpython-ads1x15
pip3 install adafruit-circuitpython-max31865
pip3 install adafruit-circuitpython-bme280
pip3 install adafruit-circuitpython-pm25

# Below requires a 64 bit OS
# curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip"
# unzip awscliv2.zip
# ./aws/install
