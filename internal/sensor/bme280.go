package sensor

import (
	"encoding/json"
)

const PyScriptBme280 = "bme280.py"

type Bme280Output struct {
	TempF    float32 `json:"temp_f"`
	Humidity float32 `json:"humidity"`
	Pressure float32 `json:"pressure"`
	Altitude float32 `json:"altitude"`
}

func Bme280ReadEnv() (float32, float32, float32, float32) {

	bme280Output := Bme280Output{}
	results := runPyScript(PyScriptBme280)

	err := json.Unmarshal([]byte(results), &bme280Output)
	if err != nil {
		panic(err)
	}

	return bme280Output.TempF, bme280Output.Humidity, bme280Output.Pressure, bme280Output.Altitude
}
