# See: https://docs.circuitpython.org/projects/pm25/en/latest/examples.html

import json
import time

import board
import busio
from adafruit_pm25.i2c import PM25_I2C

reset_pin = None

# Create library object, use 'slow' 100KHz frequency!
i2c = busio.I2C(board.SCL, board.SDA, frequency=100000)
# Connect to a PM2.5 sensor over I2C
pm25 = PM25_I2C(i2c, reset_pin)


def avg(lst):
    return sum(lst) / len(lst)


readings = {
    "pm10std": list(),
    "pm25std": list(),
    "pm100std": list(),
    "pm10env": list(),
    "pm25env": list(),
    "pm100env": list(),
    "p03um": list(),
    "p05um": list(),
    "p10um": list(),
    "p25um": list(),
    "p50um": list(),
    "p100um": list()
}

for i in range(3):
    aqdata = pm25.read()

    readings["pm10std"].append(aqdata["pm10 standard"]),
    readings["pm25std"].append(aqdata["pm25 standard"]),
    readings["pm100std"].append(aqdata["pm100 standard"]),
    readings["pm10env"].append(aqdata["pm10 env"]),
    readings["pm25env"].append(aqdata["pm25 env"]),
    readings["pm100env"].append(aqdata["pm100 env"]),
    readings["p03um"].append(aqdata["particles 03um"]),
    readings["p05um"].append(aqdata["particles 05um"]),
    readings["p10um"].append(aqdata["particles 10um"]),
    readings["p25um"].append(aqdata["particles 25um"]),
    readings["p50um"].append(aqdata["particles 50um"]),
    readings["p100um"].append(aqdata["particles 100um"])

    time.sleep(1)

reading = dict()
for key in readings.keys():
    reading[key] = avg(readings[key])

print(json.dumps(reading))
