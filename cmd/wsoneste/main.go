package main

import (
	"rpi-garden-go/internal/awsclient"
	"rpi-garden-go/internal/sensor"
)

func main() {

	TempF, Humidity, Pressure, Altitude := sensor.Bme280ReadEnv()
	P03um, P05um, P10um, P25um, P50um, P100um := sensor.ReadAirQuality()

	dynamoDbTable := awsclient.New("garden-go-sensor-data")

	dynamoDbTable.PutSensorReading("ws1bt", TempF, 1)
	dynamoDbTable.PutSensorReading("ws1bh", Humidity, 1)
	dynamoDbTable.PutSensorReading("ws1bp", Pressure, 1)
	dynamoDbTable.PutSensorReading("ws1ba", Altitude, 1)

	dynamoDbTable.PutSensorReading("ws1p03", P03um, 0)
	dynamoDbTable.PutSensorReading("ws1p05", P05um, 0)
	dynamoDbTable.PutSensorReading("ws1p10", P10um, 0)
	dynamoDbTable.PutSensorReading("ws1p25", P25um, 0)
	dynamoDbTable.PutSensorReading("ws1p50", P50um, 0)
	dynamoDbTable.PutSensorReading("ws1p100", P100um, 0)
}
