package sensor

import (
	"fmt"

	i2c "github.com/d2r2/go-i2c"
	sht3x "github.com/d2r2/go-sht3x"
)

func ReadTemperatureAndHumidity() (float32, float32) {
	// Create new connection to i2c-bus on 0 line with address 0x44.
	// Use i2cdetect utility to find device address over the i2c-bus
	i2c, err := i2c.NewI2C(0x44, 1)
	if err != nil {
		panic(err)
	}
	defer i2c.Close()

	sensor := sht3x.NewSHT3X()
	// Clear sensor settings
	err = sensor.Reset(i2c)
	if err != nil {
		panic(err)
	}

	temp, rh, err := sensor.ReadTemperatureAndRelativeHumidity(i2c, sht3x.RepeatabilityHigh)
	if err != nil {
		panic(err)
	}

	fmt.Println("Temperature and relative humidity = %v*C, %v%%", temp, rh)

	return temp, rh
}
