package sensor

import (
	"encoding/json"
	"fmt"
)

const PyScriptWindDirection = "wind_direction.py"

type WindDirectionReading struct {
	Direction float32 `json:"direction"`
}

func ReadWindDirection() float32 {

	reading := WindDirectionReading{}
	results := runPyScript(PyScriptWindDirection)

	err := json.Unmarshal([]byte(results), &reading)
	if err != nil {
		fmt.Println(results)
		panic(err)
	}

	return reading.Direction
}
