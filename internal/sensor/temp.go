package sensor

import (
	"encoding/json"
)

type Ds18b20Output struct {
	TempF float32 `json:"temp_f"`
}

type Max31865Output struct {
	TempF float32 `json:"temp_f"`
}

type Sht31dOutput struct {
	TempF    float32 `json:"temp_f"`
	Humidity float32 `json:"humidity"`
}

const PyScriptDs18b20 = "temperature.py"

var PyScriptArgsDs18b20 = []string{
	"--sensor",
	"ds18b20",
}

const PyScriptMax31865 = "temperature.py"

var PyScriptArgsMax31865 = []string{
	"--sensor",
	"max31865",
}

const PyScriptSht31d = "temperature.py"

var PyScriptArgsSht31d = []string{
	"--sensor",
	"sht31d",
}

func Ds18b20ReadTemperature() float32 {
	ds18b20Output := Ds18b20Output{}

	results := runPyScript(PyScriptDs18b20, PyScriptArgsDs18b20...)

	err := json.Unmarshal([]byte(results), &ds18b20Output)
	if err != nil {
		panic(err)
	}

	return ds18b20Output.TempF
}

func Max31865ReadTemperature() float32 {

	max31865Output := Max31865Output{}

	results := runPyScript(PyScriptMax31865, PyScriptArgsMax31865...)

	err := json.Unmarshal([]byte(results), &max31865Output)
	if err != nil {
		panic(err)
	}

	return max31865Output.TempF
}

func Sht31dReadTemperatureAndHumidity() (float32, float32) {
	sht31dOutput := Sht31dOutput{}

	results := runPyScript(PyScriptSht31d, PyScriptArgsSht31d...)

	err := json.Unmarshal([]byte(results), &sht31dOutput)
	if err != nil {
		panic(err)
	}

	return sht31dOutput.TempF, sht31dOutput.Humidity
}
