package sensor

import (
	"encoding/json"
)

const PyScriptMoistureResistive = "moisture_resistive.py"

type ResistiveMoistureSensorReading struct {
	Value   int     `json:"value"`
	Voltage float32 `json:"voltage"`
}

func ReadResistiveTemperatureAndVoltage() (float32, int) {

	resistiveMoistureSensorReading := ResistiveMoistureSensorReading{}
	results := runPyScript(PyScriptMoistureResistive)

	err := json.Unmarshal([]byte(results), &resistiveMoistureSensorReading)
	if err != nil {
		panic(err)
	}

	return resistiveMoistureSensorReading.Voltage, resistiveMoistureSensorReading.Value
}
