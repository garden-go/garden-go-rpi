import argparse
import glob
import json
import sys
import time

import adafruit_max31865
import adafruit_sht31d
import board
import digitalio


def c_to_f(temp_c):
    return temp_c * 9.0 / 5.0 + 32.0


class MAX31865:
    def __init__(self):
        spi = board.SPI()
        cs = digitalio.DigitalInOut(board.D5)  # Chip select of the MAX31865 board.
        self.sensor = adafruit_max31865.MAX31865(spi, cs, wires=3, rtd_nominal=1000.0, ref_resistor=4300.0)

    def get_temperature(self):
        return c_to_f(self.sensor.temperature)

    def get_reading(self):
        return {
            "temp_f": self.get_temperature()
        }


class SHT31D:
    def __init__(self):
        i2c = board.I2C()
        self.sensor = adafruit_sht31d.SHT31D(i2c)

    def get_temperature(self):
        return c_to_f(self.sensor.temperature)

    def get_humidity(self):
        # TODO: Determine when built-in sensor heater should be used
        return self.sensor.relative_humidity

    def get_reading(self):
        return {
            "temp_f": self.get_temperature(),
            "humidity": self.get_humidity()
        }


class DS18B20:
    def __init__(self):
        self.base_dir = '/sys/bus/w1/devices/'
        self.device_folder = glob.glob(self.base_dir + '28*')[0]
        self.device_file = self.device_folder + '/w1_slave'

    def __read_temp_raw(self):
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def get_temperature(self):
        lines = self.__read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.__read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            temp_c = float(temp_string) / 1000.0

            return c_to_f(temp_c)

    def get_reading(self):
        return {
            "temp_f": self.get_temperature()
        }


def read_all():
    max31865, sht31d, ds18b20 = MAX31865(), SHT31D(), DS18B20()

    while True:
        response = {
            "max31865": max31865.get_reading(),
            "sht31d": sht31d.get_reading(),
            "ds18b20": ds18b20.get_reading()
        }

        print(json.dumps(response, indent=4, sort_keys=True))

        time.sleep(1)


if __name__ == "__main__":
    sensor_reading_mapping = {
        "max31865": MAX31865,
        "sht31d": SHT31D,
        "ds18b20": DS18B20
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('--sensor', dest="sensor")
    parser_results = parser.parse_args()

    if not parser_results.sensor or parser_results.sensor not in sensor_reading_mapping.keys():
        print("No valid sensor specified! Exiting...")
        sys.exit(1)

    sensor = sensor_reading_mapping[parser_results.sensor]()
    sensor_reading = sensor.get_reading()

    print(json.dumps(sensor_reading))
