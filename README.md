# garden-go-rpi

&nbsp;&nbsp;&nbsp; The garden-go-rpi project contains both the weather station and plant waterer Raspberry Pi (RPi) applications for GardenGo.
Different apps can be found in the cmd/ folder.
It is written primarily in Go, but also makes use of Python helper scripts.
If possible, these helper scripts should be refactored in Go.

## Architecture

![garden-go-architecture](./documentation/gardengo.png)

### Infrastructure

This project contains the following:

- Plant watcher Go app
- Passive weather station Go app
- Active weather station Go app
- Any additional Python helper scripts

## Setup

### System - local

Please ensure the following is installed on your local machine:

- AWS CLI
- Go
- Python3

### System - target RPi

Ensure the following on the target Raspberry Pi:

- AWS CLI is installed
- Python3 is installed
- ```rpi-install-helper.sh``` has been executed

### Project

Ensure the following:

- The ```build-and-deploy.sh``` script is pointing to the correct ip, and is deploying the correct app

## Usage

### Build script

This project contains a build script that automates the deploy process. When executed, it will:

- Build & package the desired apps
- Transfer apps to the target RPi

To use, simply run:

```bash
./build-and-deploy.sh
```

## TODOs

- Update plant waterer to trigger based on an AWS event
- Refactor Python scripts to Go

## Contributing

Pull requests are welcome. To make changes, please open an issue first to discuss the change.

## License

[MIT](./LICENSE)
