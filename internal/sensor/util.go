package sensor

import "math"

func CelsiusToFahrenheit(celsius float32) float32 {
	return ((celsius * 9.0) / 5.0) + 32.0
}

func RoundFloat(float float32) float32 {
	return float32(math.Round(float64(float)*100) / 100)
}
