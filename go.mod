module rpi-garden-go

go 1.18

require (
	github.com/aws/aws-sdk-go-v2 v1.13.0 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.11.1 // indirect
	github.com/aws/aws-sdk-go-v2/credentials v1.6.5 // indirect
	github.com/aws/aws-sdk-go-v2/feature/ec2/imds v1.8.2 // indirect
	github.com/aws/aws-sdk-go-v2/internal/configsources v1.1.4 // indirect
	github.com/aws/aws-sdk-go-v2/internal/endpoints/v2 v2.2.0 // indirect
	github.com/aws/aws-sdk-go-v2/internal/ini v1.3.2 // indirect
	github.com/aws/aws-sdk-go-v2/service/dynamodb v1.11.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/accept-encoding v1.5.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/endpoint-discovery v1.3.3 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/presigned-url v1.5.2 // indirect
	github.com/aws/aws-sdk-go-v2/service/iotdataplane v1.7.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/sns v1.15.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/sso v1.7.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/sts v1.12.0 // indirect
	github.com/aws/smithy-go v1.10.0 // indirect
	github.com/d2r2/go-bh1750 v0.0.0-20181222061755-1195122364ab // indirect
	github.com/d2r2/go-i2c v0.0.0-20191123181816-73a8a799d6bc // indirect
	github.com/d2r2/go-logger v0.0.0-20210606094344-60e9d1233e22 // indirect
	github.com/d2r2/go-shell v0.0.0-20211022052110-f591c27e3e2e // indirect
	github.com/d2r2/go-sht3x v0.0.0-20181222062132-074abc261905 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/stianeikeland/go-rpio/v4 v4.6.0 // indirect
)
