## ! /usr/bin/bash

function set_plant_vars {
    RPI_IP='192.168.0.24'
    BUILD_NAME='garden-go-plant-arm-linux'
    CMD_PATH='cmd/plant/main.go'
}

function set_wszero_vars {
    RPI_IP='192.168.0.48'
    BUILD_NAME='garden-go-wszero-arm-linux'
    CMD_PATH='cmd/wszero/main.go'
}

function set_wsone_vars {
    RPI_IP='192.168.0.47'
    BUILD_NAME='garden-go-wsone-arm-linux'
    CMD_PATH='cmd/wsone/main.go'
}

function set_wsoneste_vars {
    RPI_IP='192.168.0.47'
    BUILD_NAME='garden-go-wsoneste-arm-linux'
    CMD_PATH='cmd/wsoneste/main.go'
}

go fmt rpi-garden-go/...

# set_plant_vars
# set_wszero_vars
# set_wsone_vars
set_wsoneste_vars

RPI_USER="pi"
RPI_LOGIN="$RPI_USER@$RPI_IP"

SETUP_SCRIPT="rpi-install-go.sh"
BUILD_DIR="$(pwd)/build"
BUILD_FILE="$BUILD_DIR/$BUILD_NAME"

# if [ ! -d $BUILD_DIR ] ; then
#     mkdir $BUILD_DIR
# fi

mkdir -p $BUILD_DIR

GOARCH=arm GOOS=linux GOARM=5 go build -o "$BUILD_FILE" "$CMD_PATH"
source ~/.rpi

PY_SCRIPTS_DIR='pyscripts'
PY_SCRIPTS=$(find $PY_SCRIPTS_DIR -type f)

# sshpass -p "$RPI_PASSWORD" scp $SETUP_SCRIPT "$RPI_LOGIN:/home/pi/"
#sshpass -p "$RPI_PASSWORD" scp "wsone_water.py" "$RPI_LOGIN:/home/pi/"
# sshpass -p "$RPI_PASSWORD" scp "wsone_winddirection.py" "$RPI_LOGIN:/home/pi/"
# sshpass -p "$RPI_PASSWORD" scp "wsone_windspeed.py" "$RPI_LOGIN:/home/pi/"

# sshpass -p "$RPI_PASSWORD" ssh $RPI_LOGIN "mkdir -p /home/pi/$PY_SCRIPTS_DIR"
 sshpass -p "$RPI_PASSWORD" scp $PY_SCRIPTS "$RPI_LOGIN:/home/pi/$PY_SCRIPTS_DIR"
 sshpass -p "$RPI_PASSWORD" scp $BUILD_FILE "$RPI_LOGIN:/home/pi/"

# sshpass -p "$RPI_PASSWORD" ssh $RPI_LOGIN 'sudo su && mv ./moisture_capacitive.py /usr/share/pyshared/'
# sshpass -p "$RPI_PASSWORD" ssh $RPI_LOGIN 'sudo su && mv /home/pi/moisture_capacitive.py /usr/share/pyshared/'
