package sensor

import (
	"encoding/json"
)

const PyScriptWindSpeed = "wind_speed.py"

type WindSpeedRading struct {
	WindSpeed float32 `json:"wind_speed"`
	WindGust  float32 `json:"wind_gust"`
}

func ReadWindSpeed() (float32, float32) {

	reading := WindSpeedRading{}
	results := runPyScript(PyScriptWindSpeed)

	err := json.Unmarshal([]byte(results), &reading)
	if err != nil {
		panic(err)
	}

	return reading.WindSpeed, reading.WindGust
}
