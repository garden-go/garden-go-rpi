package main

import (
	"fmt"

	"rpi-garden-go/internal/awsclient"
	"rpi-garden-go/internal/sensor"
)

func main() {

	c1 := make(chan float32)
	c2 := make(chan float32)
	c3 := make(chan float32)
	c4 := make(chan float32)

	go func() {
		rainfall, _ := sensor.ReadRainfall()
		c1 <- rainfall
	}()
	go func() {
		windDirection := sensor.ReadWindDirection()
		c2 <- windDirection
	}()
	go func() {
		windSpeed, windGust := sensor.ReadWindSpeed()
		c3 <- windSpeed
		c4 <- windGust
	}()

	rainfall := <-c1
	windDirection := <-c2
	windSpeed := <-c3
	windGust := <-c4

	dynamoDbTable := awsclient.New("garden-go-sensor-data")

	dynamoDbTable.PutSensorReading("ws1r", rainfall, 5)
	dynamoDbTable.PutSensorReading("ws1wd", windDirection, 0)
	dynamoDbTable.PutSensorReading("ws1ws", windSpeed, 2)
	dynamoDbTable.PutSensorReading("ws1wg", windGust, 2)

	fmt.Println("ws1r", rainfall)
	fmt.Println("ws1wd", windDirection)
	fmt.Println("ws1ws", windSpeed)
	fmt.Println("ws1wg", windGust)
}
