package sensor

import (
	"encoding/json"
)

// TODO: Refactor moisture py scripts to match temperature scripts syntax
const PyScriptMoistureCapacitive = "moisture_capacitive.py"

type StemmaSoilSensorPy struct {
	Moisture int     `json:"moisture"`
	TempC    float32 `json:"temp_c"`
	TempF    float32 `json:"temp_f"`
}

func ReadCapacitiveTemperatureAndMoisture() (float32, int) {

	response := runPyScript(PyScriptMoistureCapacitive)

	stemmaSoilSensorResults := StemmaSoilSensorPy{}
	err := json.Unmarshal([]byte(response), &stemmaSoilSensorResults)
	if err != nil {
		panic(err)
	}

	stemmaSoilSensorResults.TempF = CelsiusToFahrenheit(stemmaSoilSensorResults.TempC)

	return stemmaSoilSensorResults.TempF, stemmaSoilSensorResults.Moisture
}
