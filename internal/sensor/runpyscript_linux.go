package sensor

import (
	"fmt"
	"os/exec"
)

func runPyScript(scriptName string, scriptArgs ...string) string {

	rPiPath := "/home/pi/"
	scriptRelativePath := "pyscripts/"
	app := "python3"
	arg0 := rPiPath + scriptRelativePath + scriptName

	var args []string
	if len(scriptArgs) > 0 {
		args = append([]string{arg0}, scriptArgs...)
	} else {
		args = []string{
			arg0,
		}
	}

	cmd := exec.Command(app, args...)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(stdout)
		panic(err)
	}

	return string(stdout)
}
