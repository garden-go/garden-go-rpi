package main

import (
	"rpi-garden-go/internal/awsclient"
	"rpi-garden-go/internal/sensor"
)


const sensorDataTableName = "garden-go-sensor-data"

func main() {
	// Optionally use pins: 24, 23, 25
	pump := sensor.NewDcPump(18, 16, 22)
	pump.Pump(50)

	adafruitStemmaSoilSensorTemp, adafruitStemmaSoilSensorMoisture := sensor.ReadCapacitiveTemperatureAndMoisture()
	resistiveMoistureSensorVoltage, resistiveMoistureSensorValue := sensor.ReadResistiveTemperatureAndVoltage()

	dynamoDbTable := awsclient.New(sensorDataTableName)

	dynamoDbTable.PutSensorReading("p0at", adafruitStemmaSoilSensorTemp, 1)
	dynamoDbTable.PutSensorReading("p0am", float32(adafruitStemmaSoilSensorMoisture), 0)
	dynamoDbTable.PutSensorReading("p0rva", float32(resistiveMoistureSensorValue), 0)
	dynamoDbTable.PutSensorReading("p0rvo", resistiveMoistureSensorVoltage, 2)
}
