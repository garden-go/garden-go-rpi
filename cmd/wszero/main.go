package main

import (
	"rpi-garden-go/internal/awsclient"
	"rpi-garden-go/internal/sensor"
)

func main() {

	ds18b20Temperature := sensor.Ds18b20ReadTemperature()
	max31865Temperature := sensor.Max31865ReadTemperature()
	sht31dTemperature, sht31dHumidity := sensor.Sht31dReadTemperatureAndHumidity()

	dynamoDbTable := awsclient.New("garden-go-sensor-data")

	dynamoDbTable.PutSensorReading("ws0dt", ds18b20Temperature, 1)
	dynamoDbTable.PutSensorReading("ws0mt", max31865Temperature, 1)
	dynamoDbTable.PutSensorReading("ws0st", sht31dTemperature, 1)
	dynamoDbTable.PutSensorReading("ws0sh", sht31dHumidity, 1)
}
