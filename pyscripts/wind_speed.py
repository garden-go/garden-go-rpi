import json
import math
import statistics
import time

from gpiozero import Button

watch_duration = (60 * 5)
wind_interval = 1

wind_speed_sensor = Button(5)
wind_count = 0

radius_cm = 9.0
ADJUSTMENT = 1.18
CM_IN_A_KM = 100000.0
SEC_IN_A_HOUR = 3600


def spin():
    global wind_count
    wind_count = wind_count + 1


def calculate_speed(time_sec):
    global wind_count
    circumference_cm = (2 * math.pi) * radius_cm
    rotations = wind_count / 2.0

    dist_km = (circumference_cm * rotations) / CM_IN_A_KM

    km_per_sec = dist_km / time_sec
    km_per_hour = km_per_sec * SEC_IN_A_HOUR
    mi_per_hour = km_per_hour / 1.609

    return mi_per_hour * ADJUSTMENT


def reset_wind():
    global wind_count
    wind_count = 0


wind_speed_sensor.when_pressed = spin


def read_wind_speed():
    store_speeds = list()
    start_time = time.time()

    while time.time() - start_time <= watch_duration:

        interval_start_time = time.time()
        while time.time() - interval_start_time <= wind_interval:
            reset_wind()
            time.sleep(wind_interval)
            final_speed = calculate_speed(wind_interval)
            store_speeds.append(final_speed)

    wind_gust = max(store_speeds)
    wind_speed = statistics.mean(store_speeds)

    return wind_speed, wind_gust


if __name__ == "__main__":
    wind_speed, wind_gust = read_wind_speed()

    reading = {
        "wind_speed": wind_speed,
        "wind_gust": wind_gust
    }

    print(json.dumps(reading))
