package awsclient

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
)

type DynamoDbTable struct {
	tableName string
	client    *dynamodb.Client
}

const (
	ExpireInDays = 8
	Region          = "us-east-1"
)

func New(tableName string) DynamoDbTable {
	if tableName == "" {
		panic("Please specify a table name!")
	}

	// TODO: Ensure AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY os envs are set

	// Use the SDK's default configuration.
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		panic("unable to load SDK config, " + err.Error())
	}

	cfg.Region = *aws.String("us-east-1")

	// Create an Amazon DynamoDB client.
	client := dynamodb.NewFromConfig(cfg)

	return DynamoDbTable{
		tableName: tableName,
		client:    client,
	}
}

func (dbt DynamoDbTable) PutSensorReading(sensorName string, sensorReading float32, percision int) {

	floatFormat := "%." + strconv.Itoa(percision) + "f"

	currentTime := time.Now()
	expireTime := currentTime.Add(time.Hour * 24 * time.Duration(ExpireInDays))

	putItemParams := dynamodb.PutItemInput{
		TableName: &dbt.tableName,
		Item: map[string]types.AttributeValue{
			"SensorName": &types.AttributeValueMemberS{Value: sensorName},
			"UnixTs":     &types.AttributeValueMemberN{Value: strconv.FormatInt(currentTime.Unix(), 10)},
			"Reading":    &types.AttributeValueMemberS{Value: fmt.Sprintf(floatFormat, sensorReading)},
			"ExpireTs":   &types.AttributeValueMemberN{Value: strconv.FormatInt(expireTime.Unix(), 10)},
		},
	}

	_, err := dbt.client.PutItem(context.TODO(), &putItemParams)
	if err != nil {
		panic("An error occurred when putting an item, " + err.Error())
	}
}
