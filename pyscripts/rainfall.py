import json
import time

from gpiozero import Button

watch_duration = (60 * 5)
rain_sensor = Button(6)
BUCKET_SIZE = 0.2794  # 0.2794 mm tip the rain bucket
count = 0


def bucket_tipped():
    global count
    count = count + 1


def reset_rainfall():
    global count
    count = 0


rain_sensor.when_pressed = bucket_tipped

if __name__ == "__main__":
    time.sleep(watch_duration)

    reading = {
        "rainfall": count * BUCKET_SIZE,
        "unit": "mm"
    }

    print(json.dumps(reading))
