# See: https://docs.circuitpython.org/projects/bme280/en/latest/examples.html

import json
import time

import board
from adafruit_bme280 import basic as adafruit_bme280

# Create sensor object, using the board's default I2C bus.
i2c = board.I2C()  # uses board.SCL and board.SDA
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)

# change this to match the location's pressure (hPa) at sea level
bme280.sea_level_pressure = 1013.25


def c_to_f(temp_c):
    return temp_c * 9.0 / 5.0 + 32.0


def avg(lst):
    return sum(lst) / len(lst)


readings = {
    "temp_f": list(),
    "humidity": list(),
    "pressure": list(),
    "altitude": list()
}

for i in range(3):
    readings["temp_f"].append(c_to_f(bme280.temperature))
    readings["humidity"].append(bme280.relative_humidity)
    readings["pressure"].append(bme280.pressure)
    readings["altitude"].append(bme280.altitude)

    time.sleep(1)

reading = dict()
for key in readings.keys():
    reading[key] = avg(readings[key])

print(json.dumps(reading))
